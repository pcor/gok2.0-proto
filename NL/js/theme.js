var chopstick =
{
    // init, something like a constructor
    init: function()
    {
        chopstickChart();
        chopstickTooltip();
        chopstickPopover();
        chopstickTabs();
        chopstickToggle();
    }
};
