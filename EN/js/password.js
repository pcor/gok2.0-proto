$(document).ready(function() {

  $('input[id=pwd]').keyup(function() {

    // set password variable
    var pswd = $(this).val();
    //validate the length
    if ( pswd.length < 8 ) {
      $('#length').removeClass('valid').addClass('invalid');
    } else {
      $('#length').removeClass('invalid').addClass('valid');
    }
    //validate letter
    if ( pswd.match(/[A-z]/) ) {
      $('#letter').removeClass('invalid').addClass('valid');
    } else {
      $('#letter').removeClass('valid').addClass('invalid');
    }

    //validate special character
    if ( pswd.match(/[(!,%,&,@,#,€,$,£,^,*,?,.,:,;,,,(,),+,-,_,~,',"]/) ) {
      $('#specialchar').removeClass('invalid').addClass('valid');
    } else {
      $('#specialchar').removeClass('valid').addClass('invalid');
    }

    //validate capital letter
    if ( pswd.match(/[A-Z]/) ) {
      $('#capital').removeClass('invalid').addClass('valid');
    } else {
      $('#capital').removeClass('valid').addClass('invalid');
    }

    //validate number
    if ( pswd.match(/\d/) ) {
      $('#number').removeClass('invalid').addClass('valid');
    } else {
      $('#number').removeClass('valid').addClass('invalid');
    }

  }).focus(function() {
    $('#pswd_info').removeClass('inactive').addClass('active');
  }).blur(function() {
    $('#pswd_info').removeClass('active').addClass('inactive');
  });



      var pluginName = "strength",
          defaults = {
              strengthClass: 'strength',
              strengthMeterClass: 'strength_meter',
              strengthButtonClass: 'button_strength'
          };

      function Plugin( element, options ) {
          this.element = element;
          this.$elem = $(this.element);
          this.options = $.extend( {}, defaults, options );
          this._defaults = defaults;
          this._name = pluginName;
          this.init();
      }

      Plugin.prototype = {

          init: function() {


              var characters = 0;
              var capitalletters = 0;
              var loweletters = 0;
              var number = 0;
              var special = 0;

              var upperCase= new RegExp('[A-Z]');
              var lowerCase= new RegExp('[a-z]');
              var numbers = new RegExp('[0-9]');
              var specialchars = new RegExp('([!,%,&,@,#,$,^,*,?,_,~])');

              function GetPercentage(a, b) {
                      return ((b / a) * 100);
                  }

                  function check_strength(thisval,thisid){
                      if (thisval.length > 8) { characters = 1; } else { characters = 0; };
                      if (thisval.match(upperCase)) { capitalletters = 1} else { capitalletters = 0; };
                      if (thisval.match(lowerCase)) { loweletters = 1}  else { loweletters = 0; };
                      if (thisval.match(numbers)) { number = 1}  else { number = 0; };
                      if (thisval.match(specialchars)) { special = 1}  else { special = 0; };

                      var total = characters + capitalletters + loweletters + number + special;
                      var totalpercent = GetPercentage(7, total).toFixed(0);



                      get_total(total,thisid);
                  }

              function get_total(total,thisid){

                    var thismeter = $('div[id=strength_meter]');
                  if(total == 0){
                        thismeter.removeClass().html('');
                  }else if (total <= 1) {
                    thismeter.removeClass()
                    thismeter.addClass('veryweak').html('<p>Strength: very weak</p>');
                  } else if (total == 2){
                    thismeter.removeClass()
                    thismeter.addClass('weak').html('<p>Strength: weak</p>');
                  } else if(total == 3){
                    thismeter.removeClass()
                    thismeter.addClass('medium').html('<p>Strength: medium</p>');
                  } else if(total == 4){
                    thismeter.removeClass()
                    thismeter.addClass('strong').html('<p>Strength: strong</p>');
                  } else {
                    thismeter.removeClass()
                    thismeter.addClass('superstrong').html('<p>Strength: super strong</p>');
                  }
                  console.log(total);
              }


              var isShown = false;
              var strengthButtonText = this.options.strengthButtonText;
              var strengthButtonTextToggle = this.options.strengthButtonTextToggle;


              thisid = this.$elem.attr('id');


              this.$elem.bind('keyup keydown', function(event) {
                  thisval = $('#'+thisid).val();
                  $('input[type="text"][data-password="'+thisid+'"]').val(thisval);
                  check_strength(thisval,thisid);

              });


          },

      };

      // A really lightweight plugin wrapper around the constructor,
      // preventing against multiple instantiations
      $.fn[pluginName] = function ( options ) {
          return this.each(function () {
              if (!$.data(this, "plugin_" + pluginName)) {
                  $.data(this, "plugin_" + pluginName, new Plugin( this, options ));
              }
          });
      };


});
