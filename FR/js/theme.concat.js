var chopstick =
{
    // init, something like a constructor
    init: function()
    {
        chopstickChart();
        chopstickTooltip();
        chopstickPopover();
        chopstickTabs();
        chopstickToggle();
    }
};

var chopstickChart = function() {

    var _dataGlobal = {
        labels: ["Payé par Partenamut", "Payé par vous"],
        datasets: [{
            data: [1307.41, 145.26],
            backgroundColor: [
                "#FFCE56",
                "#FF6384"
            ],
            hoverBorderColor: [
                "#529ee0",
                "#529ee0"
            ],
            hoverBackgroundColor: [
                "#FFCE56",
                "#FF6384"
            ]
        }]
    };

    var _dataPersonal = {
        labels: ["Payé par Partenamut", "Payé par vous"],
        datasets: [{
            data: [81.36, 29.45],
            backgroundColor: [
                "#FFCE56",
                "#FF6384"
            ],
            hoverBorderColor: [
                "#529ee0",
                "#529ee0"
            ],
            hoverBackgroundColor: [
                "#FFCE56",
                "#FF6384"
            ]
        }]
    };

    var _options = {
        cutoutPercentage: 50,
        animateRotate: true,
        legend: {
            display: true,
            position: "bottom",
            fontSize: 13
        }
    }

    var _canvasGlobal = $("#js-expenses");
    var _canvasPersonal = $("#js-expenses-personal")

    if (_canvasGlobal.length > 0) {
        var chartGlobalExpenses = new Chart(_canvasGlobal, {
            type: 'doughnut',
            data: _dataGlobal,
            options: _options
        });
    }

    if (_canvasPersonal.length > 0) {
        var chartPersonalExpenses = new Chart(_canvasPersonal, {
            type: 'doughnut',
            data: _dataPersonal,
            options: _options
        });
    }
};

var chopstickPopover = function() {
    // Initialize all popovers
    $('[data-toggle="popover"]').popover();
    if (typeof(Storage) !== "undefined") {
        if(window.sessionStorage.getItem("hideTips") !== "true") {
            $('[data-toggle="popover-show"]').popover('show');
        }
    }
    else {
        $('[data-toggle="popover-show"]').popover('show');
    }


    // Initialize a certain popover
    // $('.js-popover-upload').popover({
    //     placement:'right',
    //     html:true,
    //  }).popover('show');

     // Bind close popover event
     $('.js-popover-close').on('click', function(e){
         $(this).closest('.popover').popover('dispose');
         if(typeof(Storage) !== "undefined") {
             window.sessionStorage.setItem("hideTips","true")
         }
     });
};

var chopstickTabs = function() {

    var _tabNavigationItems = $('.js-tabs-nav > li');

    function activate(item) {
        var targetPane = $(item.find('a').attr('href'));

        // Reset all nav items in the tab navigation
        item.siblings().each(function() {
            $(this).removeClass('is-selected');
        });

        // Set the correct nav item active
        item.addClass('is-selected');

        // Reset all panes
        targetPane.siblings().each(function() {
            $(this).hide();
        });

        // Go to target pane
        targetPane.show();
    }

    // Bind click event to tab items
    _tabNavigationItems.on('click', function(e){
        // Prevent the default action on links
        e.preventDefault();

        // Switch tab to active
        activate($(this));
    });
};

var chopstickToggle = function() {
    // The toggle is called with the '.js-toggle' class and one or more data-targets
    // Use the 'is-hidden' class to hide your elements"
    var _toggle = $('.js-toggle');

    // Toggle functionality
    _toggle.on('click', function(e){
        // The trigger
        var trigger = $(this);

        // Prevent the default action on links
        e.preventDefault();

        // Split the targets if multiple
        var targets = trigger.data('target').replace(' ', '').split(',');

        // Loop trough targets and toggle the 'is-hidden' class
        for (var i = targets.length - 1; i >= 0; i--) {
            if(targets[i]){
                // Toggle the 'is-hidden' class
                $(targets[i]).toggleClass('is-hidden');
            }
        }

        if (trigger.data('highlight')) {
            // Split the targets that need to be highlighted if multiple
            var highlight = trigger.data('highlight').replace(' ', '').split(',');

            // Loop trough targets and toggle the 'is-hidden' class
            for (var i = targets.length - 1; i >= 0; i--) {
                if(highlight[i]){

                    // Toggle the 'is-hidden' class
                    trigger.closest(highlight[i]).toggleClass('is-toggled');
                }
            }
        }

        // Add an 'is-toggled' class to the trigger.
        // Use this class to style your icons, active states, etc.
        trigger.toggleClass('is-toggled');


        return false;
    });
}

var chopstickTooltip = function() {
    $('[data-toggle="tooltip"]').tooltip();
};

chopstick.init();
